﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour {
    public static AudioManager instance = null;

    public AudioSource musicStream;
    public AudioSource soundStream;

    public AudioClip menuIntroClip;
    public AudioClip menuLoopClip;
    public AudioClip levelClip;

    public AudioClip laser;

	// Use this for initialization
	void Start () {
		if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(this);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    private void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if (scene.name.Equals(GameManager.instance.mainMenuScene))
        {
            StopCoroutine("PlayMenuLoop");
            StartCoroutine("PlayMenuLoop");
        }
        if (scene.name.Equals(GameManager.instance.levelScene))
        {
            musicStream.clip = levelClip;
            musicStream.Play();
            musicStream.loop = true;
        }
    }

    IEnumerator PlayMenuLoop()
    {
        musicStream.clip = menuIntroClip;
        musicStream.Play();
        yield return new WaitForSeconds(musicStream.clip.length);
        if (SceneManager.GetActiveScene().name.Equals(GameManager.instance.mainMenuScene) ||
            SceneManager.GetActiveScene().name.Equals(GameManager.instance.playersMenuScene) ||
            SceneManager.GetActiveScene().name.Equals(GameManager.instance.difficultyMenuScene))
        {
            musicStream.clip = menuLoopClip;
            musicStream.loop = true;
            musicStream.Play();
        }
    }

    public void PlayLaserShot()
    {
        soundStream.clip = laser;
        soundStream.Play();
    }
}
