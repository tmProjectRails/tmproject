﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TrackGenerator
{
    public class SplineMock : MonoBehaviour
    {
        private List<Vector3> spline;
        public int size = 10;
        public float scaling = 0.1f;

        // Use this for initialization
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {

        }

        public List<Vector3> getSpline()
        {
            spline = new List<Vector3>();
            for (int i = 0; i < size; i++)
            {
                spline.Add(new Vector3(scaling * i, 0, 0));
            }
            return spline;
        }
    }
}
