﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TrackGenerator
{
    public class TrackMeshGenerator : MonoBehaviour
    {
        private ProceduralSpline spline;
        private int splineSize;

        private Mesh mesh;
        private MeshCollider mc;
        private List<Vector3> vertices;
        private List<int> triangles;
        private List<Vector2> uv;

        public int resolution = 100;
        public float width = 0.2f;
        public float height = 0.4f;

        public Material mat;

        // Use this for initialization
        void Start()
        {
            mesh = GetComponent<MeshFilter>().mesh = new Mesh();
            vertices = new List<Vector3>();
            triangles = new List<int>();
            uv = new List<Vector2>();
            mc = GetComponent<MeshCollider>();

            spline = GetComponent<ProceduralSpline>();
            splineSize = spline.ControlPointCount;

            Vector3 upNormal = new Vector3(0, 1, 0);

            triangles.AddRange(new int[] {
                2, 1, 0,
                0, 3, 2
            });

            for (int i = 0; i < resolution; i++)
            {
                float curPos = (float)i / resolution;
                float nextPos = (float)(i + 1) / resolution;
                Vector3 segmentStart = spline.GetLocalPoint(curPos);
                Vector3 segmentEnd = spline.GetLocalPoint(nextPos); ;

                Vector3 segmentDirection = segmentEnd - segmentStart;
                segmentDirection.Normalize();
                Vector3 segmentRight = Vector3.Cross(upNormal, segmentDirection);
                segmentRight *= width;
                Vector3 offset = segmentRight.normalized * (width / 2);
                Vector3 br = segmentRight + upNormal * height + offset;
                Vector3 tr = segmentRight + upNormal * -height + offset;
                Vector3 bl = -segmentRight + upNormal * height + offset;
                Vector3 tl = -segmentRight + upNormal * -height + offset;

                int curTriIdx = vertices.Count;

                Vector3[] segmentVerts = new Vector3[]
                {
                    segmentStart + br,
                    segmentStart + bl,
                    segmentStart + tl,
                    segmentStart + tr,
                };
                vertices.AddRange(segmentVerts);

                Vector2[] uvs = new Vector2[]
                {
                    new Vector2(0, 0),
                    new Vector2(0, 1),
                    new Vector2(1, 1),
                    new Vector2(1, 1)
                };
                uv.AddRange(uvs);

                int[] segmentTriangles = new int[]
                {
                    curTriIdx + 2, curTriIdx + 1, curTriIdx - 3, // Left face
                    curTriIdx - 2, curTriIdx + 2, curTriIdx - 3,
                    curTriIdx + 2, curTriIdx - 2, curTriIdx - 1, // Top face
                    curTriIdx + 2, curTriIdx - 1, curTriIdx + 3,
                    curTriIdx - 1, curTriIdx - 4, curTriIdx, // Right face
                    curTriIdx - 1, curTriIdx, curTriIdx + 3,
                    curTriIdx, curTriIdx - 4, curTriIdx - 3, // Bottom face
                    curTriIdx, curTriIdx - 3, curTriIdx + 1
                };
                if (i != 0)
                {
                    triangles.AddRange(segmentTriangles);
                }
                
                if (i == resolution - 1)
                {
                    curTriIdx = vertices.Count;

                    vertices.AddRange(new Vector3[] {
                        segmentEnd + br,
                        segmentEnd + bl,
                        segmentEnd + tl,
                        segmentEnd + tr
                    });

                    uv.AddRange(new Vector2[] {
                        new Vector2(0, 0),
                        new Vector2(0, 1),
                        new Vector2(1, 1),
                        new Vector2(1, 1)
                    });

                    triangles.AddRange(new int[] {
                        curTriIdx + 2, curTriIdx + 1, curTriIdx - 3, // Left face
                        curTriIdx - 2, curTriIdx + 2, curTriIdx - 3,
                        curTriIdx + 2, curTriIdx - 2, curTriIdx - 1, // Top face
                        curTriIdx + 2, curTriIdx - 1, curTriIdx + 3,
                        curTriIdx - 1, curTriIdx - 4, curTriIdx, // Right face
                        curTriIdx - 1, curTriIdx, curTriIdx + 3,
                        curTriIdx, curTriIdx - 4, curTriIdx - 3, // Bottom face
                        curTriIdx, curTriIdx - 3, curTriIdx + 1,
                        curTriIdx + 0, curTriIdx + 1, curTriIdx + 2, //end face
					    curTriIdx + 2, curTriIdx + 3, curTriIdx + 0
                    });
                }
            }

            mesh.vertices = vertices.ToArray();
            mesh.triangles = triangles.ToArray();
            mesh.uv = uv.ToArray();
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
            mc.sharedMesh = mesh;
            gameObject.GetComponent<Renderer>().material = mat;
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
