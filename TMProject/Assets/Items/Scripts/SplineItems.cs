﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplineItems : MonoBehaviour {

    public ProceduralSpline spline;
    public int frequency;
    public Transform[] items;
    public float[] probability; // same size as items, probability between 0 and 1 that object appears, sum must be 1
    public int nbObjects;
    public float rayY;
    public static Dictionary<Vector3, Transform> listItems = new Dictionary<Vector3, Transform>();

    public void Start()
    {
        
    }

    public void CreateItems()
    {
        if (frequency <= 0 || items == null || items.Length == 0)
        {
            return;
        }
        float stepSize = 1f / (frequency * nbObjects);
        for (int p = 0, f = 0; f < frequency; f++)
        {
            for (int j = 0; j < nbObjects; j++, p++)
            {
                Vector3 position = spline.GetGlobalPoint(p * stepSize);
                position.z += spline.sizeZ * GameManager.instance.numberOfCharacters * Mathf.Sin(2 * Mathf.PI * Mathf.PerlinNoise(p + j * 12.54f, p * j + 546.9f));
                position.y += rayY * Mathf.Sin(2 * Mathf.PI * Mathf.PerlinNoise(p * 12.54f + j, p + j * 546.9f));
                position.x += Random.Range(50, 100);

                float proba = Random.Range(0f, 1f);
                float probaItem = 0;
                int i = 0;
                do
                {
                    probaItem += probability[i];
                    if (proba < probaItem)
                    {
                        Transform go = Instantiate(items[i], position, Quaternion.identity);
                        listItems.Add(go.transform.position, go);
                    }

                    i++;
                } while (probaItem < proba);
            }
        }
    }
}
