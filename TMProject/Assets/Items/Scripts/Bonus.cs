﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus : MonoBehaviour {
    public int points;
    public int upgrade;
    public int debuff;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public int GetBonus()
    {
        switch (this.tag)
        {
            case "points":
                return points;
            case "upgrade":
                return upgrade;
            case "debuff":
                return debuff;
            default:
                return 0;
        }
    }
}
