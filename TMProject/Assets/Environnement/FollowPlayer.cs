﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        this.gameObject.transform.position = new Vector3(GameManager.instance.characters[0].transform.position.x, this.gameObject.transform.position.y, this.gameObject.transform.position.z);
    }
}
