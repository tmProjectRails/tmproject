﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour {
    public int playerID; // From 0 to maxPlayers - 1
    public Transform spawnPoint;
    public int points = 0;
    public int maxPoints = 1000;
    public float speed;
    public float boostLevel = 1; // We will have a upgrade to increase the effectiveness of your boost

    private Gun gun;
    private SplineWalker sw;
    private string fire;
    private string boost;
    private int frameCount;
    private bool turnIsOver = false;

    // Use this for initialization
    void Start () {
        gun = gameObject.GetComponentInChildren<Gun>();
        gun.playerID = playerID;
        sw = gameObject.GetComponent<SplineWalker>();
        gameObject.GetComponentInChildren<DynamicCamera>().playerID = playerID;
        fire = "Fire" + (playerID + 1);
        boost = "Boost" + (playerID + 1);
        frameCount = gun.framesToSleep;

        // Split the screen depending on the number of players
        Camera cam = transform.Find("Main Camera").GetComponent<Camera>();
        switch (GameManager.instance.numberOfPlayers)
        {
            case 1:
                // Only 1 player, full screen
                if (playerID == 0)
                {
                    cam.rect = new Rect(0, 0, 1, 1);
                }
                break;
            case 2:
                // 2 players, split screen in half, horizontally or vertically
                if (GameManager.instance.verticalSplitScreen)
                {
                    switch (playerID)
                    {
                        case 0: // Top
                            cam.rect = new Rect(0, 0.5f, 1, 0.5f);
                            break;
                        case 1: // Bottom
                            cam.rect = new Rect(0, 0, 1, 0.5f);
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    switch (playerID)
                    {
                        case 0: // Left
                            cam.rect = new Rect(0, 0, 0.5f, 1);
                            break;
                        case 1: // Right
                            cam.rect = new Rect(0.5f, 0, 0.5f, 1);
                            break;
                        default:
                            break;
                    }
                }
                break;
            case 3:
                switch (playerID)
                {
                    case 0: // Top Left corner
                        cam.rect = new Rect(0, 0.5f, 0.5f, 0.5f);
                        break;
                    case 1: // Top Right corner
                        cam.rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
                        break;
                    case 2: // Bottom Left corner
                        cam.rect = new Rect(0, 0, 0.5f, 0.5f);
                        break;
                    default:
                        break;
                }
                break;
            case 4:
                switch (playerID)
                {
                    case 0: // Top Left corner
                        cam.rect = new Rect(0, 0.5f, 0.5f, 0.5f);
                        break;
                    case 1: // Top Right corner
                        cam.rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
                        break;
                    case 2: // Bottom Left corner
                        cam.rect = new Rect(0, 0, 0.5f, 0.5f);
                        break;
                    case 3: // Bottom Right corner
                        cam.rect = new Rect(0.5f, 0, 0.5f, 0.5f);
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (GameManager.gameOn)
        {


            if (Input.GetButtonDown(fire) && frameCount == gun.framesToSleep)
            {
                gun.StopCoroutine("FireLaser");
                gun.StartCoroutine("FireLaser");
                frameCount = 0;
            }
            if (frameCount < gun.framesToSleep)
            {
                frameCount++;
            }
            if (Input.GetButton(boost) && points >= GameManager.instance.boostCost)
            {
                speed = Boost(boostLevel);
                points -= GameManager.instance.boostCost;
            }
            else
            {
                if (speed <= GameManager.instance.baseSpeed)
                {
                    speed = GameManager.instance.baseSpeed;
                }
                else
                {
                    speed -= 1;
                }
            }
            sw.speed = speed;
            if (!turnIsOver && sw.progress >= 1f)
            {
                GameManager.instance.FinishTurn(playerID);
                turnIsOver = true;
            }
        }
    }

    public void Reset()
    {
        transform.position = spawnPoint.position;
        transform.rotation = spawnPoint.rotation;

        gameObject.SetActive(false);
        gameObject.SetActive(true);
    }

    public void AddPoints(int n)
    {
        points += n;
        if (points > maxPoints)
        {
            points = maxPoints;
        }
    }

    private float Boost(float level)
    {
        return sw.Boost(level);
    }
    public void StartNewTurn()
     {
         sw.progress = 0f;
         turnIsOver = false;
     }
 
}
