﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
    public int playerID;

	// Use this for initialization
	void Start () {
        Destroy(gameObject, 3);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<Bonus>() != null)
        {
            Bonus bonusItem = collision.gameObject.GetComponent<Bonus>();
            int bonus = bonusItem.GetBonus();
            switch (collision.gameObject.tag)
            {
                case "points":
                    GameManager.instance.GivePointsToPlayer(playerID, bonus);
                    break;
                case "upgrade":
                    GameManager.instance.UpgradePlayer(playerID, bonus);
                    break;
                case "debuff":
                    break;
            }
            SplineItems.listItems.Remove(collision.gameObject.transform.position);
            Destroy(collision.gameObject);
        }
        Destroy(gameObject);
    }
}
