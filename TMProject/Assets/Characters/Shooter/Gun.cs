﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {
    public Projectile projectilePrefab;
    public int framesToSleep = 30;
    public float speed = 30;
    public int playerID;
    private string fireButton;
    private Vector3 m_direction;

    public LineRenderer line;

	// Use this for initialization
	void Start () {
        line = GetComponent<LineRenderer>();
        line.enabled = false;
        fireButton = "Fire" + (playerID + 1);
    }
	
	// Update is called once per frame
	void Update () {

    }

    public void Fire()
    {
        Projectile projectile = Instantiate(projectilePrefab, transform.position, transform.rotation);
        projectile.playerID = playerID;
        Rigidbody rb = projectile.GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezeRotation;
        rb.velocity = projectile.transform.forward * speed;
    }
    
    IEnumerator FireLaser()
    {
        while (Input.GetButtonDown(fireButton))
        {
            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit hit;
            line.SetPosition(0, ray.origin);
            if (Physics.Raycast(ray, out hit, 100))
            {
                line.SetPosition(1, hit.point);
            }
            else
            {
                line.SetPosition(1, ray.GetPoint(100));
            }
            m_direction = line.GetPosition(1);
            Projectile projectile = Instantiate(projectilePrefab, transform.position, Quaternion.identity);
            projectile.transform.LookAt(m_direction);
            projectile.playerID = playerID;
            Rigidbody rb = projectile.GetComponent<Rigidbody>();
            rb.constraints = RigidbodyConstraints.FreezeRotation;
            AudioManager.instance.PlayLaserShot();
            rb.velocity = projectile.transform.forward * speed;
            Destroy(projectile, 3);
            yield return null;
        }
    }
}
