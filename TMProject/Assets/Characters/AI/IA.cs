﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IA : MonoBehaviour
{
    public Projectile projectilePrefab;
    public int framesToSleep = 30;
    public float projectileSpeed = 100;
    public float speed;

    public int playerID;
    public Difficulty difficulty;
    public Strategy strategy;
    public int points = 0;
    public int maxPoints = 1000;
    public float boostLevel = 1;

    private SplineWalker sw;
    private int frameCount;
    private bool usePoints;
    private bool turnIsOver = false;

    // Use this for initialization
    void Start()
    {
        frameCount = framesToSleep;
        sw = gameObject.GetComponent<SplineWalker>();
        if (strategy == Strategy.useAllPointsImmediatly)
        {
            usePoints = true;
        }
        else
        {
            usePoints = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.gameOn)
        {
            if (frameCount == framesToSleep)
            {
                Fire();
                frameCount = 0;
            }
            if (frameCount < framesToSleep)
            {
                frameCount++;
            }
            if (points >= GameManager.instance.boostCost && usePoints)
            {
                UsePoints();
            }
            else
            {
                if (points == maxPoints)
                {
                    usePoints = true;
                }
                if (points == 0 && strategy == Strategy.usePointsWhenFull)
                {
                    usePoints = false;
                }
                if (speed <= GameManager.instance.baseSpeed)
                {
                    speed = GameManager.instance.baseSpeed;
                }
                else
                {
                    speed -= 1;
                }
            }
            sw.speed = speed;

            if (!turnIsOver && sw.progress >= 1f)
            {
                turnIsOver = true;
                GameManager.instance.FinishTurn(playerID);
            }
        }
    }

    void Fire()
    {
        if (SplineItems.listItems.Count > 0)
        {
            List<Transform> items = new List<Transform>(SplineItems.listItems.Values);
            Transform item = items[Random.Range(0, items.Count)];
            Vector3 direction = item.transform.position - transform.position;
            int accuracy = 40 - 10 * (int)difficulty;
            float randX = Random.Range(-accuracy, accuracy);
            float randY = Random.Range(-accuracy, accuracy);
            float randZ = Random.Range(-accuracy, accuracy);
            direction = direction + new Vector3(randX, randY, randZ);
            direction = direction.normalized;
            Projectile projectile = Instantiate<Projectile>(projectilePrefab, transform.position, Quaternion.identity);
            projectile.playerID = playerID;
            Rigidbody rb = projectile.GetComponent<Rigidbody>();
            rb.constraints = RigidbodyConstraints.FreezeRotation;
            rb.velocity = direction * projectileSpeed;
            Destroy(projectile, 1f);
        }
    }

    public void AddPoints(int n)
    {
        points += n;
        if (points > maxPoints)
        {
            points = maxPoints;
        }
    }

    private float Boost(float level)
    {
        return sw.Boost(level);
    }

    private void UsePoints()
    {
            speed = Boost(boostLevel);
            points -= GameManager.instance.boostCost;
    }

    public void StartNewTurn()
    {
        sw.progress = 0f;
        turnIsOver = false;
    }
}
