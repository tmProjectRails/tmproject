﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Strategy
{
    useAllPointsImmediatly,
    usePointsWhenFull,
}
