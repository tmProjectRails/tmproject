﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Difficulty
{
    easy = 0,
    normal,
    hard,
    veryHard,
    invincible,
}
