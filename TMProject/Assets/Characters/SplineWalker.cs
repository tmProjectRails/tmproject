﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SplineWalker : MonoBehaviour {

    public ProceduralSpline spline;

    public float duration;

    public bool lookForward;

    public float progress = 0;
    public float speed;

    private void Start()
    {
    }

    private void Update()
    {
        if (GameManager.gameOn)
        {
            //progress += Time.deltaTime / duration;
            progress += Time.deltaTime * speed / 1000;
            if (progress > 1f)
            {
                progress = 1f;
            }
            Vector3 position = spline.GetGlobalPoint(progress);
            transform.localPosition = position;
            if (lookForward)
            {
                transform.LookAt(position + spline.GetDirection(progress));
            }
        }
        
    }

    public float Boost(float level)
    {
        float spe = GameManager.instance.baseSpeed + GameManager.instance.baseSpeed / 2f * (level - spline.GetDirection(progress).y);
        speed = spe;
        return spe;
    }
}
