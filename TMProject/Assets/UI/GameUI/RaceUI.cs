﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RaceUI : MonoBehaviour {

    public Text text;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (GameManager.gameOn)
        {
            text.text = "Lap    " + (4 - GameManager.instance.numberOfRemainingTurns) + "/3";
        }
    }
}
