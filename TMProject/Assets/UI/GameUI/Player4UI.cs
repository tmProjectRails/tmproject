﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player4UI : MonoBehaviour
{

    public Slider slider;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.gameOn)
        {
            slider.value = GameManager.instance.characters[3].GetComponent<SplineWalker>().progress;
        }
    }
}
