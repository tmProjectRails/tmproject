﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsUI : MonoBehaviour {
    public int playerId;
    public Text text;

    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (GameManager.gameOn)
        {
            text.text = "Points : " + GameManager.instance.GetPoints(playerId);
        }
    }
}
