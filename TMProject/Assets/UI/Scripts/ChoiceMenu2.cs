﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class ChoiceMenu2 : MonoBehaviour
{

    public void OneBtn()
    {
        GameManager.instance.difficulty = Difficulty.easy;
    }

    public void TwoBtn()
    {
        GameManager.instance.difficulty = Difficulty.normal;
    }

    public void ThreeBtn()
    {
        GameManager.instance.difficulty = Difficulty.hard;
    }

    public void FourBtn()
    {
        GameManager.instance.difficulty = Difficulty.veryHard;
    }

    public void FiveBtn()
    {
        GameManager.instance.difficulty = Difficulty.invincible;
    }

    public void OkBtn(string PlayScene)
    {
        SceneManager.LoadScene(PlayScene);
    }

    public void CancelBtn(string MenuScene)
    {
        SceneManager.LoadScene(MenuScene);
    }

}
