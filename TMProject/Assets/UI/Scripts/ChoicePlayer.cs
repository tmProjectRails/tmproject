﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



public class ChoicePlayer : MonoBehaviour
{

    private int players = 1;

    public void OneBtn()
    {
        GameManager.instance.numberOfPlayers = 1;
    }

    public void TwoBtn()
    {
        GameManager.instance.numberOfPlayers = 2;
    }

    public void ThreeBtn()
    {
        GameManager.instance.numberOfPlayers = 3;
    }

    public void FourBtn()
    {
        GameManager.instance.numberOfPlayers = 4;
    }


    public void OkBtn(string PlayScene)
    {
        SceneManager.LoadScene(PlayScene);
    }

    public void CancelBtn(string MenuScene)
    {
        SceneManager.LoadScene(MenuScene);
    }

}
