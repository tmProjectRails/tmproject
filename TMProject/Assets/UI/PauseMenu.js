﻿#pragma strict

var MenuObject : GameObject;
private var isActive : boolean = false;

function Start(){
	Cursor.visible = false;
}

function Update () {
	if(isActive == true){
		MenuObject.SetActive(true);
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.None;
		Time.timeScale = 0;
	}
	else{
		MenuObject.SetActive(false);
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Locked;
		Time.timeScale = 1;
	}
	if(Input.GetKeyDown(KeyCode.Escape)){
		RESUME_BUTTON();
	}
}

function RESUME_BUTTON(){
	isActive = !isActive;
}

function SETTINGS(){
	MenuObject.SetActive(false);
}