﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicCamera : MonoBehaviour {
    public int playerID;
    public float rotationX;
    public float rotationY;
    public float sensitivity = 5f;
    public bool activ = true;
    private string inputX;
    private string inputY;
	// Use this for initialization
	void Start () {
        inputX = "input X " + (playerID + 1);
        inputY = "input Y " + (playerID + 1);
	}
	
	// Update is called once per frame
	void Update () {
        if (activ)
        {
            rotationX -= Input.GetAxis(inputY) * sensitivity;
            rotationY += Input.GetAxis(inputX) * sensitivity;

            transform.rotation = Quaternion.Euler(rotationX, rotationY, 0);
        }
        
    }

    public void off()
    {
        activ = false;
    }

    public void on()
    {
        activ = true;
    }
}
