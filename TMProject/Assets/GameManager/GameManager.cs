﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    public static GameManager instance = null;

    public GameObject[] characters;

    public GameObject trackPrefab;
    public GameObject playerPrefab;
    public GameObject[] enemyPrefab;
    public GameObject itemsPrefab;
    public int maxPlayers = 4;
    public int numberOfPlayers = 1;
    public bool verticalSplitScreen = false;
    public int numberOfCharacters;
    public Difficulty difficulty;
    public float baseSpeed = 10;
    public int boostCost = 1;
    public Canvas infos;
    public Canvas target;

    public string levelScene = "MainLevel";
    public string mainMenuScene = "MainMenu";
    public string playersMenuScene = "PlayersMenu";
    public string difficultyMenuScene = "DifficultyMenuScene";

    public Material[] playerColors;

    static public bool gameOn = false;

    private Dictionary<int, float> playersTime = new Dictionary<int, float>();
    private float turnStartTime;
    private int numberOfFinishTurn = 0;
    public int numberOfRemainingTurns = 3;
    private int level;

    // Use this for initialization
    void Awake() {
        if (GameManager.instance == null)
        {
            GameManager.instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
	}
	
	// Update is called once per frame
	void Update () {
        
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    private void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if (scene.name.Equals(levelScene))
        {
            StartGame();
        }
    }

    void StartGame()
    {
        characters = new GameObject[numberOfCharacters];

        //numberOfPlayers = PlayerPrefs.GetInt("nb_players", 1);
        //level = PlayerPrefs.GetInt("difficulty", 0);
        switch (level)
        {
            case 1:
                difficulty = Difficulty.easy;
                break;
            case 2:
                difficulty = Difficulty.normal;
                break;
            case 3:
                difficulty = Difficulty.hard;
                break;
            case 4:
                difficulty = Difficulty.veryHard;
                break;
            case 5:
                difficulty = Difficulty.invincible;
                break;
            default:
                difficulty = Difficulty.easy;
                break;
        }
        ProceduralSpline.perlin = Random.Range(1, 1000);
        // Player 1 must be a human player
        Vector3 posPlayer = new Vector3(0.0f, 0.0f, 0.0f);
        GameObject traPlayer = Instantiate(trackPrefab, posPlayer, Quaternion.identity);
        traPlayer.GetComponent<TrackGenerator.TrackMeshGenerator>().mat = playerColors[0];
        GameObject pl = Instantiate(playerPrefab, posPlayer, Quaternion.identity);

        ProceduralSpline splinePlayer = traPlayer.GetComponent<ProceduralSpline>();
        pl.GetComponent<SplineWalker>().spline = splinePlayer;
        pl.GetComponent<PlayerManager>().playerID = 0;
        pl.GetComponent<PlayerManager>().speed = baseSpeed;
        characters[0] = pl;
        
        Canvas Target = Instantiate(target, posPlayer, Quaternion.identity);
        Canvas UI = Instantiate(infos, posPlayer, Quaternion.identity);
        Target.renderMode = RenderMode.ScreenSpaceCamera;
        Target.worldCamera = pl.GetComponentInChildren<Camera>();
        UI.renderMode = RenderMode.ScreenSpaceCamera;
        UI.worldCamera = pl.GetComponentInChildren<Camera>();
        UI.GetComponentInChildren<JaugeUI>().playerId = 0;
        UI.GetComponentInChildren<PointsUI>().playerId = 0;

        playersTime.Add(0, 0f);

        for (int i = 1; i < numberOfCharacters; i++)
        {
            Vector3 pos = new Vector3(0.0f, 0.0f, splinePlayer.sizeZ * (float)i);
            GameObject track = Instantiate(trackPrefab, pos, Quaternion.identity);
            track.GetComponent<TrackGenerator.TrackMeshGenerator>().mat = playerColors[i];
            GameObject chara;
            if (i < numberOfPlayers) // if character is a human player
            {
                chara = Instantiate(playerPrefab, pos, Quaternion.identity);
                chara.GetComponent<PlayerManager>().playerID = i;
                chara.GetComponent<PlayerManager>().speed = baseSpeed;
                chara.GetComponentInChildren<AudioListener>().enabled = false;

                Canvas Target1 = Instantiate(target, pos, Quaternion.identity);
                Canvas UI1 = Instantiate(infos, pos, Quaternion.identity);
                Target1.renderMode = RenderMode.ScreenSpaceCamera;
                Target1.worldCamera = chara.GetComponentInChildren<Camera>();
                UI1.renderMode = RenderMode.ScreenSpaceCamera;
                UI1.worldCamera = chara.GetComponentInChildren<Camera>();
                UI1.GetComponentInChildren<JaugeUI>().playerId = i;
                UI1.GetComponentInChildren<PointsUI>().playerId = i;
            }
            else // if character is an AI
            {
                chara = Instantiate(enemyPrefab[i - 1], pos, Quaternion.identity);
                chara.GetComponent<IA>().playerID = i;
                chara.GetComponent<IA>().speed = baseSpeed;
                chara.GetComponent<IA>().difficulty = difficulty;
            }
            chara.GetComponent<SplineWalker>().spline = track.GetComponent<ProceduralSpline>();
            characters[i] = chara;

            if (i == (int)numberOfCharacters / 2)
            {
                GameObject it = Instantiate(itemsPrefab);
                it.GetComponent<SplineItems>().spline = track.GetComponent<ProceduralSpline>();
                it.GetComponent<SplineItems>().CreateItems();
            }
            playersTime.Add(i, 0f);
        }
        gameOn = true;
        turnStartTime = Time.time;
    }

    public void GivePointsToPlayer(int playerID, int points)
    {
        if (playerID < numberOfPlayers)
        {
            characters[playerID].GetComponent<PlayerManager>().AddPoints(points);
        }
        else
        {
            characters[playerID].GetComponent<IA>().AddPoints(points);
        }
    }

    public void UpgradePlayer(int playerID, int upgrade)
    {
        if (playerID < numberOfPlayers)
        {
            characters[playerID].GetComponent<PlayerManager>().boostLevel+=upgrade;
        }
        else
        {
            characters[playerID].GetComponent<IA>().boostLevel+=upgrade;
        }
    }

    public void DebuffPlayer(int playerID, int debuff)
    {
        for (int i = 0; i < numberOfCharacters; i++)
        {
            if (i != playerID)
            {
                if (i < numberOfPlayers && characters[i].GetComponent<PlayerManager>().boostLevel > 1)
                {
                    characters[i].GetComponent<PlayerManager>().boostLevel -= debuff;
                }
                else if (characters[i].GetComponent<IA>().boostLevel > 1)
                {
                    characters[i].GetComponent<IA>().boostLevel -= debuff;
                }
            }
        }
    }

    public void FinishTurn(int playerId)
    {
        playersTime[playerId] += Time.time - turnStartTime;
        numberOfFinishTurn++;

        if (numberOfFinishTurn >= numberOfCharacters)
        {
            gameOn = false;
            numberOfRemainingTurns--;
            if (numberOfRemainingTurns > 0)
            {
                StartNewTurn();
            }
        }
    }

    public void StartNewTurn()
    {
        for (int i = 0; i < numberOfCharacters; i++)
        {
            if (i < numberOfPlayers)
            {
                characters[i].GetComponent<PlayerManager>().StartNewTurn();
            }
            else
            {
                characters[i].GetComponent<IA>().StartNewTurn();
            }
        }

        numberOfFinishTurn = 0;
        gameOn = true;
        turnStartTime = Time.time;
    }

    public int GetPoints(int playerID)
    {
        if (playerID < numberOfPlayers)
        {
            return characters[playerID].GetComponent<PlayerManager>().points;
        }
        else
        {
            return characters[playerID].GetComponent<IA>().points;
        }
    }

    public void OnGUI()
    {
        if (numberOfRemainingTurns == 0)
        {
            GUI.backgroundColor = Color.black;
            GUI.contentColor = Color.white;
            string results = "TIME :\n";
            for (int i = 0; i < numberOfCharacters; i++)
            {
                results = results + "Player " + i + " : " + playersTime[i] + " s\n";
            }
            GUI.Label(new Rect(Screen.width / 2, Screen.height / 2, 300, 100), results);
        }
    }
}
